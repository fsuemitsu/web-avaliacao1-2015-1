package utfpr.faces.support;

import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */



@Named
@RequestScoped
public class LoginBean{

    private String usuario;
    private String senha;
    private String opadmin;
    private Date data;



    public LoginBean() {
    }

    public LoginBean(String usuario, Date data, String senha, String opadmin) {
       this.data = data;
       this.opadmin = opadmin;
       this.senha = senha;
       this.usuario = usuario;
    }

    public String getOpadmin() {
        return opadmin;
    }

    public void setOpadmin(String opadmin) {
        this.opadmin = opadmin;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String entrar() {
        System.out.println("OK");
        if (usuario.equals(senha)) {
            data = new Date();
            if(opadmin.equals("true")){
                LoginListBean.getInstance().addLoginBeanList(new LoginBean(usuario, data, senha, opadmin));
                return "admin";
            }else{
                return "cadastro";
            }
        } else {
            FacesMessage msg = new FacesMessage("Acesso negado");
            FacesContext.getCurrentInstance().addMessage("erro", msg);
            return null;
        }
    }
}
