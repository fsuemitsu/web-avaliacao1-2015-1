package utfpr.faces.support;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author Wilson
 */
@Named
@ApplicationScoped
public class LoginListBean {
    
    private ArrayList <LoginBean> loginBeanList = new ArrayList <>();
    
    private static LoginListBean instance = null;
    
    public static LoginListBean getInstance(){
        if(instance==null){
            instance = new LoginListBean();
        }
        return instance;
    }

    public LoginListBean() {
    }

    public ArrayList <LoginBean> getLoginBeanList() {
        return loginBeanList;
    }
    
    public void setLoginBeanList(ArrayList <LoginBean> loginBeanList) {
        this.loginBeanList = loginBeanList;
    }

    void addLoginBeanList(LoginBean loginBean) {
       this.loginBeanList.add(loginBean);
    }
}
